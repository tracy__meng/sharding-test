# shardingTest

# shardingsphere 5.0.0-alpha版本搭建读写分离项目示例。
# 此案例数据库为mysql
# mysql数据库需要做好主从复制
# 修改yml文件的url为自己的数据库连接即可。
# 数据库表结构
# DROP TABLE IF EXISTS `userTest`;
# CREATE TABLE `userTest` (
#   `id` int NOT NULL AUTO_INCREMENT,
#   `name` varchar(20) DEFAULT '',
#   `sex` varchar(2) DEFAULT '',
#   `age` int DEFAULT NULL,
#   `createTime` datetime DEFAULT NULL,
#   PRIMARY KEY (`id`)
# ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
