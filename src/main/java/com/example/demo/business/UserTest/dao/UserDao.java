package com.example.demo.business.UserTest.dao;

import com.example.demo.business.UserTest.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Diammd
 */
@Mapper
public interface UserDao {

    public List<User> getUser();

    public void addUser(User user);
}
