package com.example.demo.business.UserTest.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @program: User
 * @description:
 * @author: Diammd
 * @create: 2021-08-09 09:42
 */
@Data
public class User {
    private int id;
    private String name;
    private String sex;
    private int age;
    private Date createTime;
}
