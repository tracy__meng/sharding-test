package com.example.demo.business.UserTest.controller;

import com.example.demo.business.UserTest.pojo.User;
import com.example.demo.business.UserTest.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: UserTest
 * @description:
 * @author: Diammd
 * @create: 2021-08-09 09:39
 */
@RestController
@RequestMapping("UserTest")
public class UserTest {

    @Autowired
    private UserServices userServices;

    @GetMapping(value = "/getUser")
    public List<User> getUser(){
        return userServices.getUser();
    }

    @GetMapping(value = "/addUser")
    public void addUser(){
        User user = new User();
        user.setName("张三");
        user.setAge(19);
        user.setSex("男");
        userServices.addUser(user);
    }
}
