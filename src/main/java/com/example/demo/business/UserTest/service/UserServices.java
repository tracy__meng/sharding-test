package com.example.demo.business.UserTest.service;

import com.example.demo.business.UserTest.dao.UserDao;
import com.example.demo.business.UserTest.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: UserServices
 * @description:
 * @author: Diammd
 * @create: 2021-08-09 10:10
 */
@Service
public class UserServices {

    @Autowired
    private UserDao userDao;

    public List<User> getUser(){
        return userDao.getUser();
    }

    public void addUser(User user){
        userDao.addUser(user);
    }

}
